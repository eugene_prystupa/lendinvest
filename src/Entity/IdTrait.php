<?php

namespace App\Entity;

trait IdTrait
{
    /**
     * @var string
     */
    private $id;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return void
     */
    private function generateId()
    {
        $this->id = md5(time());
    }
}