<?php

namespace App\Entity;

use App\Exception\AmountException;

class Money
{
    use IdTrait;

    /**
     * @var float
     */
    private $amount;

    /**
     * Money constructor.
     *
     * @param float $amount
     */
    public function __construct(float $amount = 0)
    {
        $this->amount = $amount;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     *
     * @return $this
     */
    public function increaseAmount(float $amount): self
    {
        $this->amount = $this->amount + $amount;
        return $this;
    }

    /**
     * @param float $amount
     *
     * @return $this
     * @throws AmountException
     */
    public function decreaseAmount(float $amount): self
    {
        if ($this->amount < $amount) {
            throw new AmountException('Got negative balance');
        }

        $this->amount = $this->amount - $amount;
        return $this;
    }
}