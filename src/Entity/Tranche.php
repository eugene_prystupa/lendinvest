<?php

namespace App\Entity;

class Tranche implements AvailabilityInterface
{
    use IdTrait;

    /**
     * @var Loan
     */
    private $loan;

    /**
     * @var Money
     */
    private $amount;

    /**
     * @var float
     */
    private $percent;

    /**
     * @var array|Payment[]
     */
    private $payments = [];

    /**
     * @var Money
     */
    private $availableAmount;

    /**
     * Tranche constructor.
     *
     * @param Loan  $loan
     * @param Money $amount
     * @param float $percent
     */
    public function __construct(Loan $loan, Money $amount, float $percent)
    {
        $this->loan = $loan;
        $this->amount = $amount;
        $this->percent = $percent;
        $this->generateId();
    }

    /**
     * @return Loan
     */
    public function getLoan(): Loan
    {
        return $this->loan;
    }

    /**
     * @return Money
     */
    public function getAmount(): Money
    {
        return $this->amount;
    }

    /**
     * @return float
     */
    public function getPercent(): float
    {
        return $this->percent;
    }

    /**
     * @return array
     */
    public function getPayments(): array
    {
        return $this->payments;
    }

    /**
     * @return Money
     */
    public function getAvailableAmount(): Money
    {
        if (null === $this->availableAmount) {

            $paymentsAmount = 0;
            foreach ($this->getPayments() as $payment) {
                $paymentsAmount =+ $payment->getMoney()->getAmount();
            }

            $this->availableAmount = new Money($this->getAmount()->getAmount() - $paymentsAmount);
        }

        return $this->availableAmount;
    }

    /**
     * @param Payment $payment
     *
     * @return Tranche
     * @throws \App\Exception\AmountException
     */
    public function addPayment(Payment $payment): self
    {
        $this->getAvailableAmount()->decreaseAmount($payment->getMoney()->getAmount());

        $this->payments[] = $payment;

        return $this;
    }

    /**
     * @param Payment $payment
     *
     * @return Tranche
     */
    public function removePayment(Payment $payment): self
    {
        $this->payments = array_filter($this->payments, function (Payment $p) use ($payment) {
           return $p->getId() != $payment->getId();
        });

        $this->getAvailableAmount()->increaseAmount($payment->getMoney()->getAmount());

        return $this;
    }

    /**
     * @param \DateTime|null $paymentDate
     *
     * @return bool
     */
    public function isOpen(?\DateTime $paymentDate): bool
    {
        return $this->getLoan()->isOpen($paymentDate);
    }

    /**
     * @return bool
     */
    public function hasAvailableAmount(): bool
    {
        return ($this->getAvailableAmount()->getAmount() > 0);
    }
}