<?php

namespace App\Entity;

class Wallet
{
    use IdTrait;

    /**
     * @var Investor
     */
    private $investor;

    /**
     * @var Money
     */
    private $money;

    /**
     * Wallet constructor.
     *
     * @param Investor $investor
     * @param Money    $money
     */
    public function __construct(Investor $investor, Money $money)
    {
        $this->investor = $investor;
        $this->money = $money;
    }

    /**
     * @return Investor
     */
    public function getInvestor(): Investor
    {
        return $this->investor;
    }

    /**
     * @return Money
     */
    public function getMoney(): Money
    {
        return $this->money;
    }
}