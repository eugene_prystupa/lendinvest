<?php

namespace App\Entity;

class Investor
{
    use IdTrait;

    /**
     * @var Wallet
     */
    private $wallet;

    /**
     * @var array|Payment[]
     */
    private $payments = [];

    /**
     * @var \DateTime
     */
    private $percentCalculationDate;

    /**
     * @param Wallet $wallet
     *
     * @return Investor
     */
    public function setWallet(Wallet $wallet): self
    {
        $this->wallet = $wallet;
        return $this;
    }

    /**
     * @return Wallet
     */
    public function getWallet(): Wallet
    {
        return $this->wallet;
    }

    /**
     * @param Payment $payment
     *
     * @return Investor
     */
    public function addPayment(Payment $payment): self
    {
        $this->payments[] = $payment;
        return $this;
    }

    /**
     * @param Payment $payment
     *
     * @return Investor
     */
    public function removePayment(Payment $payment): self
    {
        $this->payments = array_filter($this->payments, function (Payment $p) use ($payment) {
            return $p->getId() != $payment->getId();
        });

        return $this;
    }

    /**
     * @return array
     */
    public function getPayments(): array
    {
        return $this->payments;
    }

    /**
     * @param \DateTime $dateTime
     *
     * @return Investor
     */
    public function setPercentCalculationDate(\DateTime $dateTime): self
    {
        $this->percentCalculationDate = $dateTime;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPercentCalculationDate():? \DateTime
    {
        return $this->percentCalculationDate;
    }
}