<?php

namespace App\Entity;

interface AvailabilityInterface
{
    /**
     * @param \DateTime|null $paymentDate
     *
     * @return bool
     */
    public function isOpen(?\DateTime $paymentDate): bool;

    /**
     * @return bool
     */
    public function hasAvailableAmount(): bool;
}