<?php

namespace App\Entity;

class Payment
{
    use IdTrait;

    /**
     * @var Investor
     */
    private $investor;

    /**
     * @var Tranche
     */
    private $tranche;

    /**
     * @var Money
     */
    private $money;

    /**
     * @var \DateTime
     */
    private $dateTime;

    /**
     * Payment constructor.
     *
     * @param Investor  $investor
     * @param Tranche   $tranche
     * @param Money     $money
     * @param \DateTime $dateTime
     */
    public function __construct(Investor $investor, Tranche $tranche, Money $money, \DateTime $dateTime)
    {
        $this->investor = $investor;
        $this->tranche = $tranche;
        $this->money = $money;
        $this->dateTime = $dateTime;
        $this->generateId();
    }

    /**
     * @return Investor
     */
    public function getInvestor(): Investor
    {
        return $this->investor;
    }

    /**
     * @return Tranche
     */
    public function getTranche(): Tranche
    {
        return $this->tranche;
    }

    /**
     * @return Money
     */
    public function getMoney(): Money
    {
        return $this->money;
    }

    /**
     * @return \DateTime
     */
    public function getDateTime(): \DateTime
    {
        return $this->dateTime;
    }
}