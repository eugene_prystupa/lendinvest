<?php

namespace App\Entity;

class Loan implements AvailabilityInterface
{
    use IdTrait;

    /**
     * @var \DateTime
     */
    private $startDate;

    /**
     * @var \DateTime
     */
    private $finishDate;

    /**
     * @var array|Tranche[]
     */
    private $tranches;

    /**
     * Loan constructor.
     *
     * @param \DateTime $startDate
     * @param \DateTime $finishDate
     */
    public function __construct(\DateTime $startDate, \DateTime $finishDate)
    {
        $this->startDate = $startDate;
        $this->finishDate = $finishDate;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate(): \DateTime
    {
        return $this->startDate;
    }

    /**
     * @return \DateTime
     */
    public function getFinishDate(): \DateTime
    {
        return $this->finishDate;
    }

    /**
     * @return array
     */
    public function getTranches(): array
    {
        return $this->tranches;
    }

    /**
     * @param Tranche $tranche
     *
     * @return Loan
     */
    public function addTranche(Tranche $tranche): self
    {
        $this->tranches[] = $tranche;
        return $this;
    }

    /**
     * @param Tranche $tranche
     *
     * @return Loan
     */
    public function removeTranche(Tranche $tranche): self
    {
        $this->tranches = array_filter($this->tranches, function (Tranche $t) use ($tranche) {
            return $t->getId() != $tranche->getId();
        });

        return $this;
    }

    /**
     * @param \DateTime|null $paymentDate
     *
     * @return bool
     */
    public function isOpen(?\DateTime $paymentDate): bool
    {
        $paymentDate = ($paymentDate) ?? new \DateTime();
        return ($this->getStartDate()->getTimestamp() <= $paymentDate->getTimestamp() && $paymentDate->getTimestamp() <= $this->getFinishDate()->getTimestamp());
    }

    /**
     * @return bool
     */
    public function hasAvailableAmount(): bool
    {
        foreach ($this->getTranches() as $tranche) {
            if ($tranche->hasAvailableAmount()) {
                return true;
            }
        }

        return false;
    }
}