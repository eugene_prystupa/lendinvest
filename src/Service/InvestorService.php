<?php

namespace App\Service;

use App\Entity\Investor;
use App\Entity\Money;
use App\Entity\Payment;
use App\Entity\Tranche;
use App\Exception\AmountException;
use App\Exception\AvailabilityException;

class InvestorService
{
    /**
     * @param Investor       $investor
     * @param Tranche        $tranche
     * @param Money          $amount
     * @param \DateTime|null $paymentDate
     *
     * @return bool
     * @throws AmountException
     * @throws AvailabilityException
     */
    public function investMoney(Investor $investor, Tranche $tranche, Money $amount, ?\DateTime $paymentDate): bool
    {
        if (!$tranche->isOpen($paymentDate) || !$tranche->hasAvailableAmount()) {
            throw new AvailabilityException('Tranche is close or not has available amount to invest');
        }

        try {

            $investor->getWallet()->getMoney()->decreaseAmount($amount->getAmount());

            $payment = new Payment($investor, $tranche, $amount, ($paymentDate) ?? new \DateTime());
            $tranche->addPayment($payment);
            $investor->addPayment($payment);
        } catch (AmountException $exception) {

            if (isset($payment)) {
                $investor->removePayment($payment);
                $tranche->removePayment($payment);
            }

            throw $exception;
        }

        return true;
    }

    /**
     * @param Investor       $investor
     * @param \DateTime|null $dateTime
     *
     * @return bool
     */
    public function calculatePercentAmount(Investor $investor, ?\DateTime $dateTime): bool
    {
        $dateTime = ($dateTime) ?? new \DateTime();

        $prevMonthStart  = (clone $dateTime)->modify('FIRST DAY OF PREVIOUS MONTH');
        $prevMonthFinish = (clone $dateTime)->modify('LAST DAY OF PREVIOUS MONTH');

        /**
         * Suppose that we calculate tranche percent amount one time a month. If amount was calculated once we shouldn't calculate it again. So, we save percent calculation date
         */
        if ($investor->getPercentCalculationDate() &&
            !($prevMonthStart->getTimestamp() < $investor->getPercentCalculationDate()->getTimestamp() &&
              $investor->getPercentCalculationDate()->getTimestamp() < $prevMonthFinish->getTimestamp())) {

            return false;
        }

        /**
         * Take investor payments that was made during last payment period. Calculate percent for each such payment.
         */
        foreach ($investor->getPayments() as $payment) {
            if ($prevMonthStart->getTimestamp() < $payment->getDateTime()->getTimestamp() && $payment->getDateTime()->getTimestamp() <= $prevMonthFinish->getTimestamp()) {

                /**
                 * Define number of days for month. Define payment period.
                 */
                $daysByPayment = $prevMonthFinish->diff($payment->getDateTime())->format('%d') + 1;
                $daysInMonth = $prevMonthFinish->diff($prevMonthStart)->format('%d') + 1;

                /**
                 * Calculate payment percent and add it to investor wallet
                 */
                $investor->getWallet()->getMoney()->increaseAmount(round($payment->getMoney()->getAmount() * ($daysByPayment / $daysInMonth) * ($payment->getTranche()->getPercent() / 100), 2));
            }
        }

        $investor->setPercentCalculationDate($prevMonthFinish);

        return true;
    }
}