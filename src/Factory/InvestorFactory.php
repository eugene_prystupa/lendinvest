<?php

namespace App\Factory;

use App\Entity\Investor;
use App\Entity\Money;
use App\Entity\Wallet;

class InvestorFactory
{
    /**
     * @param Money|null $money
     *
     * @return Investor
     */
    public static function createInvestor(?Money $money): Investor
    {
        $investor = new Investor();
        $investor->setWallet(new Wallet($investor, ($money) ?? new Money()));

        return $investor;
    }
}