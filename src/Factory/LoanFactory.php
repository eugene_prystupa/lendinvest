<?php

namespace App\Factory;

use App\Entity\Loan;
use App\Entity\Money;
use App\Entity\Tranche;
use App\Exception\ValidationException;

class LoanFactory
{
    /**
     * @param \DateTime $startDate
     * @param \DateTime $finishDate
     * @param array     $tranchesData has structure [0 => ['amount' => Money, 'percent' => float]]
     *
     * @return Loan
     * @throws ValidationException
     */
    public static function createLoan(\DateTime $startDate, \DateTime $finishDate, array $tranchesData = []): Loan
    {
        $loan = new Loan($startDate, $finishDate);
        if (!empty($tranchesData)) {

            foreach ($tranchesData as $trancheData) {
                if (!isset($trancheData['amount']) || !($trancheData['amount'] instanceof Money) || !isset($trancheData['percent'])) {
                    throw new ValidationException('Not valid tranches data during loan creation');
                }

                $loan->addTranche(self::createTranche($loan, $trancheData['amount'], $trancheData['percent']));
            }
        }

        return $loan;
    }

    /**
     * @param Loan  $loan
     * @param Money $amount
     * @param float $percent
     *
     * @return Tranche
     */
    public static function createTranche(Loan $loan, Money $amount, float $percent): Tranche
    {
        return new Tranche($loan, $amount, $percent);
    }
}