<?php

namespace App\Test;

use App\Entity\Money;
use App\Exception\AmountException;
use App\Exception\AvailabilityException;
use App\Service\InvestorService;
use Fixture\InvestorBuilder;
use Fixture\LoanBuilder;
use PHPUnit\Framework\TestCase;

class ApplicationTest extends TestCase
{
    public function test()
    {
        /**
         * Create loan with two tranches
         */
        $loan = LoanBuilder::createLoanWithTwoTranches();
        $trancheA = $loan->getTranches()[0];
        $trancheB = $loan->getTranches()[1];

        /**
         * Create four investors
         */
        $investor1 = InvestorBuilder::createInvestor(1000);
        $investor2 = InvestorBuilder::createInvestor(1000);
        $investor3 = InvestorBuilder::createInvestor(1000);
        $investor4 = InvestorBuilder::createInvestor(1000);

        $investorService = new InvestorService();

        /**
         * Invest money for first tranche. Second investor gets exception
         */
        $investorService->investMoney($investor1, $trancheA, new Money(1000), new \DateTime('2015-10-03'));
        try {
            $investorService->investMoney($investor2, $trancheA, new Money(1), new \DateTime('2015-10-04'));
        } catch (\Exception $exception) {;
            $this->assertTrue(get_class($exception) === AvailabilityException::class);
        }

        /**
         * Invest money for second tranche. Fourth investor gets exception.
         */
        $investorService->investMoney($investor3, $trancheB, new Money(500), new \DateTime('2015-10-10'));
        try {
            $investorService->investMoney($investor4, $trancheB, new Money(1100), new \DateTime('2015-10-25'));
        } catch (\Exception $exception) {
            $this->assertTrue(get_class($exception) === AmountException::class);
        }

        /**
         * Calculate percent for first investor and check his new balance
         */
        $investorService->calculatePercentAmount($investor1, new \DateTime('2015-11-01'));
        $this->assertEquals(28.06, $investor1->getWallet()->getMoney()->getAmount());

        /**
         * Calculate percent for third investor and check his new balance
         */
        $investorService->calculatePercentAmount($investor3, new \DateTime('2015-11-01'));
        $this->assertEquals(521.29, $investor3->getWallet()->getMoney()->getAmount());
    }
}