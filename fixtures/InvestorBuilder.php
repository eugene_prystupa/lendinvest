<?php

namespace Fixture;

use App\Entity\Investor;
use App\Entity\Money;
use App\Factory\InvestorFactory;

class InvestorBuilder
{
    /**
     * @param float $amount
     *
     * @return Investor
     */
    public static function createInvestor(float $amount): Investor
    {
        return InvestorFactory::createInvestor(new Money($amount));
    }
}