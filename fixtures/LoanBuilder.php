<?php

namespace Fixture;

use App\Entity\Loan;
use App\Entity\Money;
use App\Factory\LoanFactory;

class LoanBuilder
{
    /**
     * @return \App\Entity\Loan
     * @throws \App\Exception\ValidationException
     */
    public static function createLoanWithTwoTranches(): Loan
    {
        $tranchesData = [
            ['amount' => new Money(1000), 'percent' => 3],
            ['amount' => new Money(1000), 'percent' => 6]
        ];
        $loan = LoanFactory::createLoan(new \DateTime('2015-10-01'), new \DateTime('2015-11-15'), $tranchesData);

        return $loan;
    }
}